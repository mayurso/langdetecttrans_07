#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 12 10:45:07 2018

Objective - [1] Detection of source of langauage
[2] translation into final "English language"

"""

import goslate
import translate
import time
from translate import Translator
from googletrans import Translator
import csv
import re
import os
import shutil
import logging
import sys


def language(srcLanguage):
    langCode = {
        'af': 'afrikaans',
        'sq': 'albanian',
        'am': 'amharic',
        'ar': 'arabic',
        'hy': 'armenian',
        'az': 'azerbaijani',
        'eu': 'basque',
        'be': 'belarusian',
        'bn': 'bengali',
        'bs': 'bosnian',
        'bg': 'bulgarian',
        'ca': 'catalan',
        'ceb': 'cebuano',
        'ny': 'chichewa',
        'zh-cn': 'chinese (simplified)',
        'zh-tw': 'chinese (traditional)',
        'co': 'corsican',
        'hr': 'croatian',
        'cs': 'czech',
        'da': 'danish',
        'nl': 'dutch',
        'en': 'english',
        'eo': 'esperanto',
        'et': 'estonian',
        'tl': 'filipino',
        'fi': 'finnish',
        'fr': 'french',
        'fy': 'frisian',
        'gl': 'galician',
        'ka': 'georgian',
        'de': 'german',
        'el': 'greek',
        'gu': 'gujarati',
        'ht': 'haitian creole',
        'ha': 'hausa',
        'haw': 'hawaiian',
        'iw': 'hebrew',
        'hi': 'hindi',
        'hmn': 'hmong',
        'hu': 'hungarian',
        'is': 'icelandic',
        'ig': 'igbo',
        'id': 'indonesian',
        'ga': 'irish',
        'it': 'italian',
        'ja': 'japanese',
        'jw': 'javanese',
        'kn': 'kannada',
        'kk': 'kazakh',
        'km': 'khmer',
        'ko': 'korean',
        'ku': 'kurdish (kurmanji)',
        'ky': 'kyrgyz',
        'lo': 'lao',
        'la': 'latin',
        'lv': 'latvian',
        'lt': 'lithuanian',
        'lb': 'luxembourgish',
        'mk': 'macedonian',
        'mg': 'malagasy',
        'ms': 'malay',
        'ml': 'malayalam',
        'mt': 'maltese',
        'mi': 'maori',
        'mr': 'marathi',
        'mn': 'mongolian',
        'my': 'myanmar (burmese)',
        'ne': 'nepali',
        'no': 'norwegian',
        'ps': 'pashto',
        'fa': 'persian',
        'pl': 'polish',
        'pt': 'portuguese',
        'pa': 'punjabi',
        'ro': 'romanian',
        'ru': 'russian',
        'sm': 'samoan',
        'gd': 'scots gaelic',
        'sr': 'serbian',
        'st': 'sesotho',
        'sn': 'shona',
        'sd': 'sindhi',
        'si': 'sinhala',
        'sk': 'slovak',
        'sl': 'slovenian',
        'so': 'somali',
        'es': 'spanish',
        'su': 'sundanese',
        'sw': 'swahili',
        'sv': 'swedish',
        'tg': 'tajik',
        'ta': 'tamil',
        'te': 'telugu',
        'th': 'thai',
        'tr': 'turkish',
        'uk': 'ukrainian',
        'ur': 'urdu',
        'uz': 'uzbek',
        'vi': 'vietnamese',
        'cy': 'welsh',
        'xh': 'xhosa',
        'yi': 'yiddish',
        'yo': 'yoruba',
        'zu': 'zulu',
        'fil': 'Filipino',
        'he': 'Hebrew'
    }

    return langCode[srcLanguage]


def get_logger(filename, log_file=None):
    logger = logging.getLogger(filename)
    logger.setLevel(logging.INFO)

    formatter = logging.Formatter('%(asctime)s - [%(name)s:%(lineno)d] - %(levelname)s - %(message)s')

    channel = logging.FileHandler(log_file) if log_file else \
        logging.StreamHandler(sys.stdout)
    channel.setLevel(logging.INFO)
    channel.setFormatter(formatter)

    logger.addHandler(channel)

    return logger

def total_word_count(data):
    wordCount = 0
    for i in range(0, len(data)):
        wordCount = wordCount + len(data[i])

    return wordCount


def language_translator(content_of_document):
    from prettytable import PrettyTable

    translator = Translator()

    translations = translator.translate(content_of_document, dest='en')

    translated_document = list()

    mytable = PrettyTable(["Entity", "Output"])

    for translation in translations:
        # print(translation.origin, ' -> ', translation.text)
        trans_text = translation.text;
        transLangName = language(DetectLang(trans_text)).upper()
        translated_document.append(trans_text)

        orig_text = translation.origin
        orig_lang = DetectLang(orig_text)
        origLangName = language(orig_lang).upper()

    wc1 = total_word_count(content_of_document)
    wc2 = total_word_count(translated_document)

    mytable.add_row(["Origin Language", origLangName])
    mytable.add_row(["Translated Language", transLangName])
    mytable.add_row(["Word Count Original Document", wc1])
    mytable.add_row(["Word Count Translated Document", wc2])

    print(mytable)

    return translated_document, transLangName


def write_output(dirPath, FileName, doc):
    """
    OUTPUT /EXPORT FILE SAVED HERE - DESTINATION DOCUMENT MUST BE ENGLISH
    """

    outFileName = dirPath + FileName + '.txt'

    with open(outFileName, "w") as output:
        writer = csv.writer(output, lineterminator='\n')
        for val in doc:
            writer.writerow([val])


def DetectLang(word):
    word_cnt = len(word)
    if word_cnt > 15000:
        print()

    return Translator().detect(word).lang


"""
************************* MAIN CODE ******************************************
"""

if __name__ == '__main__':
    startTime = time.time()
    logger = get_logger(__file__)
    dirPath = "C:/Users/Mayur Sonawane/Documents/unstructured/cleaned/text/"
    destpath = "C:/Users/Mayur Sonawane/Documents/unstructured/transalted/"

    if not os.path.exists(destpath):
        os.makedirs(destpath)

    for file in os.listdir(dirPath):
        try:
            filename = os.fsdecode(file)

            #print('Translating the file: {}'.format(filename))

            with open(str(dirPath) + str(filename), 'r', encoding='UTF-8') as readfile:
                content_of_document = readfile.read()

            originalDocument = re.sub(r'[^\w]', ' ', str(content_of_document))
            #print('detecting lang')
            originalLanguage = DetectLang(originalDocument)

            #print('detecting lang complete')

            if (language(originalLanguage) != 'english'):
                #print('Document not in english')
                translated_document, transLangName = language_translator(content_of_document)
                write_output(dirPath, filename, translated_document)
            else:
                #print('Document in english!')
                shutil.move(str(dirPath) + str(filename), str(destpath) + str(filename))
        except Exception as e:
            # exception happened
            #print('exception occured: {}'.format(e))
            logger.info("Unable to Translate file: {} of char_length{}: {}".format(filename,len(originalDocument), e))